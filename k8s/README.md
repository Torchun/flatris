## Create deployments and loadBalancer:
```
kubectl create -f run_flatris.yaml
```

## Monitor resources status:
```
kubectl get svc
kubectl get deployments
kubectl get pods
```

## Stop and remove k8s resources:
```
kubectl scale deployment --replicas=0 flatris-pod
kubectl delete -f run_flatris.yaml
```
